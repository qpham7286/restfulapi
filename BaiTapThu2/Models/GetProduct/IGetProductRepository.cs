﻿namespace BaiTapThu2.Models.GetProduct
{
    public interface IGetProductRepository
    {
        Task<IEnumerable<Product>> GetProductsAsync();
    }
}
