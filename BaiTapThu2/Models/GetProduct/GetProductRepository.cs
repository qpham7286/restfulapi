﻿using Microsoft.EntityFrameworkCore;

namespace BaiTapThu2.Models.GetProduct
{
    public class GetProductRepository : IGetProductRepository
    {
        private readonly ApplicationDbContext _context;
        public GetProductRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<Product>> GetProductsAsync()
        {
            return await _context.Products.ToListAsync();
        }
    }
}
